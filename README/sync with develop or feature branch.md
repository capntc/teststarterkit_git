# Sync with develop/feature branch

## Context

Sometimes you may want to use other work. For example you would like to see in your code what others already did. Or you want to get latest develop version to
start working on new feature. In that case you need to sync with desired branch. Sync means pulling changes from other branch and merge it with your code.

## Steps

1. identify branches you want to sync
2. switch to source branch
3. pull changes from target branch
4. resolve conflicts (if any)

## Example

###### Assumptions:

* I am on `feature/michal-git-book` branch and would like to sync with `develop` branch.
* `feature/michal-git-book` is source branch, and `develop` is target branch

### Console

1. commit or stash any existing changes
2. type `git pull origin develop` to pull changes from target branch:

```
$ git pull origin develop
From https://bitbucket.org/capntc/gitintroduction
 * branch            develop    -> FETCH_HEAD
Merge made by the 'recursive' strategy.
```

3. Enter commit message.

4. Sometimes at this time you can get an error with conflicting changes. You need to resolve all conflicts before going further.

And that`s it. Now you are proud owner of latest develop code.

### SourceTree

1. commit or stash any existing changes
2. Right click on `develop` branch and select `Pull origin/develop' into current branch
   > Make sure you right click on a branch from `Remotes origin` section, not local `Branches` section
   > ![](./ST-screen-shots/SyncWithDevelop1.png)
3. Click "OK"
   ![](./ST-screen-shots/SyncWithDevelop2.png)
   > Resolve conflicts (if any). Remember to commit changes after resolving conflicts
4. Now you have changes from develop branch merged into your branch
5. This line will cause conflicts. And now is Thursday, I forgot about monday and have added this. Lets merge!
