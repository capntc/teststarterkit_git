# Test Starter Kit - EXERCISES
>
> To pass this git course all tasks below must be done
>
> To pass this git course all applied solutions should be known

## Context
We are working in travel agency on hotels database. In this moment agency offers only 2 hotels in each country from Greece,Italy and Spain. These hotels can be seen on `develop` branch in hotels directory. 

Hotels database structure is kept in json files according to template:
```
 {
  "hotel1Name": {
    "location": "hotelAddress",
    "rooms": "numberOfRooms",
    "isSPA": "true|false",
    "attractions": [
      "attraction1",
      "attraction2,
      ...,
      "attractionN"
    ]
  },
  "hotel2Name": {
      "location": "hotelAddress",
      "rooms": "numberOfRooms",
      "isSPA": "true|false",
      "attractions": [
        "attraction1",
        "attraction2,
        ...,
        "attractionN"
      ]
    }
 }
 ```

### Task 1
> committing

1. switch to existing branch `testStarterKit/fornameLastname/task1`
2. add commit  with 3 files (greece.json,italy.json,spain.json) with content `{}`
3. add 3 hotels in 3 commits (one per file)
4. publish changes on remote repository

### Task 2
> pull request

**task 2 should be made after finishing task 1**

1. create `testStarterKit/fornameLastname/task2` branch being on branch `testStarterKit/fornameLastname/task1`
2. add 3 hotels in 3 commits (one per existing file)
3. create pull request with target branch `testStarterKit/fornameLastname/task1` 

### Task 3
> reverting changes

1. create branch `testStarterKit/fornameLastname/task3` from develop branch
2. add 3 hotels in 3 commits (one per existing file)
3. revert 3 just added commits with default commit messages (one revert per commit)
4. publish your branch on remote repository

### Task 4
> conflits resolving

1. switch to existing branch `testStarterKit/fornameLastname/task4`
2. add 3 hotels in 3 commits (one per existing file)
3. create free of conflicts pull request with develop as target branch 